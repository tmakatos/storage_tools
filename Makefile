
DCCEXE=dcc
DCCSTATIC=dcc.static
DCCSRC=dcc

BTOREXE=btor
BTORSRC=btorture

all: $(DCCEXE) $(BTOREXE)

$(DCCEXE)::
	(cd $(DCCSRC) ; make $(TARGET))
	# cp $(DCCSRC)/$(DCCSTATIC) $(DCCSRC)/$(DCCEXE) .

$(BTOREXE)::
	(cd $(BTORSRC) ; make $(TARGET))
	# cp $(BTORSRC)/$(BTORSTATIC) $(BTORSRC)/$(BTOREXE) .

.PHONY: clean install

clean:
	(cd $(DCCSRC) ; make $@)
	(cd $(BTORSRC) ; make $@)

install:
	(cd $(DCCSRC) ; make $@)
	(cd $(BTORSRC) ; make $@)


